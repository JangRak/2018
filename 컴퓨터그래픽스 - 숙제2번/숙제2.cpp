#include <gl/freeglut.h>
#include <math.h>
#include <random>
#include <ctime>
#include <iostream>

using namespace std;

// ----------------------------------- 열거 자료형 -----------------------------------

enum WORK { ON, OFF };									// 동작여부
enum VIEW { TOP, SIDE };								// 탑뷰 / 사이드뷰
enum COLLISIONCHECK { YES, NO };						// 충돌체크
enum MOUSE { DOWN, UP };								// 마우스 클릭
enum POINTOFVIEW { ROLLER, ROB, THIRD };				// 시점
enum DIRECTION { FORWARD, BACK, RIGHT, LEFT, STOP };	// 방향
enum WEATHER { SUNNY, RAIN, SNOW };						// 날씨

// ---------------------------------------------------------------------------------

// ----------------------------------- 전역 변수 -----------------------------------

// 사이즈
const double Width = 800.0;
const double Height = 600.0;

// define
#define NumberOfPoint 10
#define NumberOfRobot 2
#define NumberOfBullet 30
#define SnowRainSize 500
#define Bullet_Velocity 20.0

// like bool
int Camera = OFF;
int View = TOP;
int RightMouse = UP;
int GameStart = OFF;
int CAN_START = OFF;
int Collision = OFF;
int Point_of_View = THIRD;
int Weather = SUNNY;
int Collsion_Something = NO;
int Can_game_start_about_collision = NO;

// 롤러코스터 제어점 카운트
int Count_controlpoint_of_rollercoaster = 0;
int Count_Column_of_rollercoaster = 0;
const int Number_Column_of_rollercoaster = NumberOfPoint / 2;

// 총알 개수
int Count_Bullet = -1;

// 열차그리기 위한 변수
double rollercoaster_controlpoint_xPosArr[NumberOfPoint] = { 0, };
double rollercoaster_controlpoint_yPosArr[NumberOfPoint] = { 0, };
double rollercoaster_controlpoint_zPosArr[NumberOfPoint] = { 0, };

double UpX = 0.0, UpY = 1.0, UpZ = 0.0, EyeX = 0.0, EyeY = 10.0, EyeZ = 100.0, CenterX = 0.0, CenterY = 0.0, CenterZ = 0.0;	// 카메라
double Camera_X = 0.0, Camera_Y = 0.0, Camera_Z = 0.0;
double ViewRotateX = 90.0, ViewRotateY = 0.0, ViewRotateZ = 0.0;	// 초기 뷰 (카메라 OFF)
double far_Camera_zPos = -800.0;

int Main_Window;		// 종료를 위한 변수

int CreateTunnel = 0;

// -----------------------------------------------------------------------------------

// ----------------------------------- 공유 데이터 -----------------------------------
typedef struct SHAREDDATA
{
	SHAREDDATA() {}
	SHAREDDATA(double xPos, double yPos, double zPos) : xPos(xPos), yPos(yPos), zPos(zPos) {}
	SHAREDDATA(double xPos, double yPos, double zPos, double RotateValue, double RotateX, double RotateY, double RotateZ) :
		xPos(xPos), yPos(yPos), zPos(zPos), RotateValue(0.0), RotateX(0.0), RotateZ(0.0) {}
	~SHAREDDATA() {}
	double xPos;			// 좌표
	double yPos;
	double zPos;

	double RotateValue;		// 회전
	double RotateX;
	double RotateY;
	double RotateZ;

}SharedData;

// ----------------------------------- 롤러코스터 제어점 -----------------------------------
class CONTROLPOINT_OF_ROLLERCOASTER
{
public:
	CONTROLPOINT_OF_ROLLERCOASTER() : Collision(NO), Collision_with_obstacle(NO) {}
	~CONTROLPOINT_OF_ROLLERCOASTER() {}

public:
	// Set 함수
	void Set_xPos(int xPos) { Data.xPos = (double)xPos - (Width / 2.0); }				// xPos Set
	void Set_yPos(int yPos) { Data.yPos = Height / 2.0 - (double)yPos; }				// yPos Set
	void Set_zPos(int zPos) { Data.zPos = -(Height / 2.0 - (double)zPos); }				// zPos Set
	void SetCollision(int state) { Collision = state; }									// Collision Set
	void Set_Collision_with_obstacle(int state) { Collision_with_obstacle = state; }	// Collision_with_obstacle Set

	// Get 함수
	double Get_xPos() const { return Data.xPos; }								// xPos Get
	double Get_yPos() const { return Data.yPos; }								// yPos Get
	double Get_zPos() const { return Data.zPos; }								// zPos Get
	int GetCollision() const { return Collision; }								// Collision Get
	int GetCollision_with_obstacle() const { return Collision_with_obstacle; }	// Collision_with_obstacle Get

	// 오른쪽 마우스 클릭시 충돌 여부
	void CollisionWithMouse(int xPos, int yPos)
	{
		double Mouse_xPos = (double)xPos - (Width / 2.0);
		double Mouse_yPos = Height / 2.0 - (double)yPos;
		double Mouse_zPos = -(Height / 2.0 - (double)yPos);

		if (Mouse_xPos < Data.xPos + 10.0 && Mouse_xPos > Data.xPos - 10.0 &&
			Mouse_yPos < Data.yPos + 10.0 && Mouse_yPos > Data.yPos - 10.0 && View == SIDE)			// 사이드뷰 => x-x, y-y
			Collision = YES;

		else if (Mouse_xPos < Data.xPos + 10.0 && Mouse_xPos > Data.xPos - 10.0 &&
			Mouse_zPos < Data.zPos + 10.0 && Mouse_zPos > Data.zPos - 10.0 && View == TOP)		// 탑뷰 => x-x, y-z
			Collision = YES;
	}

	void MovePos(int xPos, int yPos)
	{
		if (Collision == YES) {
			if (View == SIDE) {
				Data.yPos = Height / 2.0 - (double)yPos;
				if (Data.yPos < -100.0)
					Data.yPos = -100.0;
			}

			else if (View == TOP) {
				Data.xPos = (double)xPos - (Width / 2.0);
				Data.zPos = -(Height / 2.0 - (double)yPos);
			}
		}
	}

private:
	SharedData Data;
	int Collision;
	int Collision_with_obstacle;
};

// ----------------------------------- 롤러코스터 열차 -----------------------------------
class CAR
{
public:
	CAR() : Car_Draw_Count_0(15.0), Car_Draw_Count_1(0.0), Car_Draw_Count_2(85.0), Car_Count_0(0), Car_Count_1(0), Car_Count_2(9) {}
	~CAR() {}

public:
	void Set_Pos(double* xPos, double* yPos, double* zPos)
	{
		for (int i = 0; i < NumberOfPoint; ++i) {
			xPosArr[i] = xPos[i];
			yPosArr[i] = yPos[i];
			zPosArr[i] = zPos[i];
		}
	}

	void Define_Camera_Pos()
	{
		xPos_for_Eye = Car_0_xPos;
		yPos_for_Eye = Car_0_yPos;
		zPos_for_Eye = Car_0_zPos;

		//if (Car_0_yPos > Car_1_yPos) {
		//	if (Car_0_yPos - 20.0 > Car_1_yPos) {
		//		zPos_for_Up = yPos_for_Up = 1.0;
		//		xPos_for_Up = 0.0;
		//	}
		//}
		//else if (Car_0_yPos < Car_1_yPos) {
		//	if (Car_0_yPos + 20.0 < Car_1_yPos) {
		//		zPos_for_Up = -1.0;
		//		yPos_for_Up = 1.0;
		//		xPos_for_Up = 0.0;
		//	}
		//}
		//else {
		//	yPos_for_Up = 1.0;
		//	xPos_for_Up = zPos_for_Up = 0.0;
		//}
	}

	void Draw()	// 열차그리기
	{
		glPushMatrix();
		Cal_Pos_and_Draw(Car_Count_0, Car_Draw_Count_0, Car_0_xPos, Car_0_yPos, Car_0_zPos);
		Cal_Pos_and_Draw(Car_Count_1, Car_Draw_Count_1, Car_1_xPos, Car_1_yPos, Car_1_zPos);
		Cal_Pos_and_Draw(Car_Count_2, Car_Draw_Count_2, Car_2_xPos, Car_2_yPos, Car_2_zPos);
		Cal_Camera_Center_Pos(Car_Count_0, Car_Draw_Count_0, Car_0_xPos, Car_0_yPos, Car_0_zPos);
		glPopMatrix();
		Define_Camera_Pos();
	}

	void Cal_Camera_Center_Pos(int Car_Count, double Car_Draw_Count, double xPos, double yPos, double zPos)
	{
		double t = Car_Draw_Count / 100.0;

		xPos = ((-(t*t*t) + 2 * (t*t) - t) * xPosArr[Car_Count % NumberOfPoint] + (3 * (t*t*t) - 5 * (t*t) + 2) * xPosArr[(Car_Count + 1) % NumberOfPoint] + (-3 * (t*t*t) + 4 * (t*t) + t) * xPosArr[(Car_Count + 2) % NumberOfPoint] + ((t*t*t) - (t*t)) * xPosArr[(Car_Count + 4) % NumberOfPoint]) / 2;
		yPos = ((-(t*t*t) + 2 * (t*t) - t) * yPosArr[Car_Count % NumberOfPoint] + (3 * (t*t*t) - 5 * (t*t) + 2) * yPosArr[(Car_Count + 1) % NumberOfPoint] + (-3 * (t*t*t) + 4 * (t*t) + t) * yPosArr[(Car_Count + 2) % NumberOfPoint] + ((t*t*t) - (t*t)) * yPosArr[(Car_Count + 4) % NumberOfPoint]) / 2;
		zPos = ((-(t*t*t) + 2 * (t*t) - t) * zPosArr[Car_Count % NumberOfPoint] + (3 * (t*t*t) - 5 * (t*t) + 2) * zPosArr[(Car_Count + 1) % NumberOfPoint] + (-3 * (t*t*t) + 4 * (t*t) + t) * zPosArr[(Car_Count + 2) % NumberOfPoint] + ((t*t*t) - (t*t)) * zPosArr[(Car_Count + 4) % NumberOfPoint]) / 2;

		xPos_for_Center = xPos;
		yPos_for_Center = yPos;
		zPos_for_Center = zPos;
	}

	void Cal_Pos_and_Draw(int& Car_Count, double& Car_Draw_Count, double& xPos, double& yPos, double& zPos)
	{
		double t = Car_Draw_Count / 100.0;

		xPos = ((-(t*t*t) + 2 * (t*t) - t) * xPosArr[Car_Count % NumberOfPoint] + (3 * (t*t*t) - 5 * (t*t) + 2) * xPosArr[(Car_Count + 1) % NumberOfPoint] + (-3 * (t*t*t) + 4 * (t*t) + t) * xPosArr[(Car_Count + 2) % NumberOfPoint] + ((t*t*t) - (t*t)) * xPosArr[(Car_Count + 4) % NumberOfPoint]) / 2;
		yPos = ((-(t*t*t) + 2 * (t*t) - t) * yPosArr[Car_Count % NumberOfPoint] + (3 * (t*t*t) - 5 * (t*t) + 2) * yPosArr[(Car_Count + 1) % NumberOfPoint] + (-3 * (t*t*t) + 4 * (t*t) + t) * yPosArr[(Car_Count + 2) % NumberOfPoint] + ((t*t*t) - (t*t)) * yPosArr[(Car_Count + 4) % NumberOfPoint]) / 2;
		zPos = ((-(t*t*t) + 2 * (t*t) - t) * zPosArr[Car_Count % NumberOfPoint] + (3 * (t*t*t) - 5 * (t*t) + 2) * zPosArr[(Car_Count + 1) % NumberOfPoint] + (-3 * (t*t*t) + 4 * (t*t) + t) * zPosArr[(Car_Count + 2) % NumberOfPoint] + ((t*t*t) - (t*t)) * zPosArr[(Car_Count + 4) % NumberOfPoint]) / 2;


		double next_t = CountUp(Car_Draw_Count, Car_Count);

		double next_x = ((-(next_t*next_t*next_t) + 2 * (next_t*next_t) - next_t) * xPosArr[Car_Count % NumberOfPoint] + (3 * (next_t*next_t*next_t) - 5 * (next_t*next_t) + 2) * xPosArr[(Car_Count + 1) % NumberOfPoint] + (-3 * (next_t*next_t*next_t) + 4 * (next_t*next_t) + next_t) * xPosArr[(Car_Count + 2) % NumberOfPoint] + ((next_t*next_t*next_t) - (next_t*next_t)) * xPosArr[(Car_Count + 4) % NumberOfPoint]) / 2;
		double next_y = ((-(next_t*next_t*next_t) + 2 * (next_t*next_t) - next_t) * yPosArr[Car_Count % NumberOfPoint] + (3 * (next_t*next_t*next_t) - 5 * (next_t*next_t) + 2) * yPosArr[(Car_Count + 1) % NumberOfPoint] + (-3 * (next_t*next_t*next_t) + 4 * (next_t*next_t) + next_t) * yPosArr[(Car_Count + 2) % NumberOfPoint] + ((next_t*next_t*next_t) - (next_t*next_t)) * yPosArr[(Car_Count + 4) % NumberOfPoint]) / 2;
		double next_z = ((-(next_t*next_t*next_t) + 2 * (next_t*next_t) - next_t) * zPosArr[Car_Count % NumberOfPoint] + (3 * (next_t*next_t*next_t) - 5 * (next_t*next_t) + 2) * zPosArr[(Car_Count + 1) % NumberOfPoint] + (-3 * (next_t*next_t*next_t) + 4 * (next_t*next_t) + next_t) * zPosArr[(Car_Count + 2) % NumberOfPoint] + ((next_t*next_t*next_t) - (next_t*next_t)) * zPosArr[(Car_Count + 4) % NumberOfPoint]) / 2;

		double angle_z = atan2((next_y - yPos), sqrt((next_x - xPos) * (next_x - xPos) + (next_z - zPos) * (next_z - zPos))) * 180.0 / 3.14;
		double angle_y = atan2((next_x - xPos), (next_z - zPos)) * 180.0 / 3.14;
		double angle_x = atan2((next_z - zPos), (next_y - yPos)) * 180.0 / 3.14;

		glPushMatrix();

		glTranslatef(xPos, yPos + 40.0, zPos);
		glRotated(angle_z, 0.0, 0.0, 1.0);
		glRotated(angle_y, 0.0, 1.0, 0.0);
		glRotated(angle_x, 1.0, 0.0, 0.0);

		glColor3f(0.0f, 0.0f, 1.0f);
		glutSolidCube(80.0);
		glColor3f(0.0f, 0.0f, 0.0f);

		glPopMatrix();
	}

	// 속도조절, 로봇만들기, 선로와 장애물 충돌체크
	double CountUp(double& Car_Draw_Count, int& Car_Count)
	{
		if (Car_0_yPos < Car_1_yPos)
			Car_Draw_Count += 8.0;
		else if (Car_0_yPos > Car_1_yPos)
			Car_Draw_Count += 3.0;
		else if (Car_0_yPos == Car_1_yPos)
			Car_Draw_Count += 8.0;

		if (Car_Draw_Count > 100.0) {
			Car_Draw_Count = 0.0;
			++Car_Count;
			if (Car_Count >= NumberOfPoint)
				Car_Count = 0;
		}

		double t = Car_Draw_Count / 100.0;

		xPos_for_Center = ((-(t*t*t) + 2 * (t*t) - t) * xPosArr[Car_Count % NumberOfPoint] + (3 * (t*t*t) - 5 * (t*t) + 2) * xPosArr[(Car_Count + 1) % NumberOfPoint] + (-3 * (t*t*t) + 4 * (t*t) + t) * xPosArr[(Car_Count + 2) % NumberOfPoint] + ((t*t*t) - (t*t)) * xPosArr[(Car_Count + 4) % NumberOfPoint]) / 2;
		yPos_for_Center = ((-(t*t*t) + 2 * (t*t) - t) * yPosArr[Car_Count % NumberOfPoint] + (3 * (t*t*t) - 5 * (t*t) + 2) * yPosArr[(Car_Count + 1) % NumberOfPoint] + (-3 * (t*t*t) + 4 * (t*t) + t) * yPosArr[(Car_Count + 2) % NumberOfPoint] + ((t*t*t) - (t*t)) * yPosArr[(Car_Count + 4) % NumberOfPoint]) / 2;
		zPos_for_Center = ((-(t*t*t) + 2 * (t*t) - t) * zPosArr[Car_Count % NumberOfPoint] + (3 * (t*t*t) - 5 * (t*t) + 2) * zPosArr[(Car_Count + 1) % NumberOfPoint] + (-3 * (t*t*t) + 4 * (t*t) + t) * zPosArr[(Car_Count + 2) % NumberOfPoint] + ((t*t*t) - (t*t)) * zPosArr[(Car_Count + 4) % NumberOfPoint]) / 2;
		return t;
	}

	double Get_xPos_for_Eye() const { return xPos_for_Eye; }
	double Get_yPos_for_Eye() const { return yPos_for_Eye; }
	double Get_zPos_for_Eye() const { return zPos_for_Eye; }

	double Get_xPos_for_Center() const { return xPos_for_Center; }
	double Get_yPos_for_Center() const { return yPos_for_Center; }
	double Get_zPos_for_Center() const { return zPos_for_Center; }

	double Get_xPos_for_Up() const { return xPos_for_Up; }
	double Get_yPos_for_Up() const { return yPos_for_Up; }
	double Get_zPos_for_Up() const { return zPos_for_Up; }

	double Get_Car_1_xPos() const { return Car_1_xPos; }
	double Get_Car_1_yPos() const { return Car_1_yPos; }
	double Get_Car_1_zPos() const { return Car_1_zPos; }

	double Get_Car_0_xPos() const { return Car_0_xPos; }
	double Get_Car_0_yPos() const { return Car_0_yPos; }
	double Get_Car_0_zPos() const { return Car_0_zPos; }

private:
	double xPosArr[NumberOfPoint];
	double yPosArr[NumberOfPoint];
	double zPosArr[NumberOfPoint];

	double Car_0_xPos;
	double Car_0_yPos;
	double Car_0_zPos;

	double Car_1_xPos;
	double Car_1_yPos;
	double Car_1_zPos;

	double Car_2_xPos;
	double Car_2_yPos;
	double Car_2_zPos;

	double Car_Draw_Count_0;
	double Car_Draw_Count_1;
	double Car_Draw_Count_2;

	double xPos_for_Eye;
	double yPos_for_Eye;
	double zPos_for_Eye;

	double xPos_for_Center;
	double yPos_for_Center;
	double zPos_for_Center;

	double xPos_for_Up;
	double yPos_for_Up;
	double zPos_for_Up;

	int Car_Count_0;
	int Car_Count_1;
	int Car_Count_2;
};

//// ----------------------------------- 롤러코스터 제이점간 곡선 -----------------------------------
//class CURVE
//{
//public :
//	CURVE() : t(0), xPos(0.0), yPos(0.0), zPos(0.0) {}
//	~CURVE() {}
//
//public :
//	void Draw_Cardinal_Spline(double xPos1, double yPos1, double zPos1, double xPos2, double yPos2, double zPos2, 
//							  double xPos3, double yPos3, double zPos3, double xPos4, double yPos4, double zPos4)
//	{
//		for (int i = 0; i <= 100; i += 2) {
//			t = i / 100;
//			xPos = ((-(t*t*t) + 2 * (t*t) - t) * xPos1 + (3 * (t*t*t) - 5 * (t*t) + 2) * xPos2 + (-3 * (t*t*t) + 4 * (t*t) + t) * xPos3 + ((t*t*t) - (t*t)) * xPos4) / 2;
//			yPos = ((-(t*t*t) + 2 * (t*t) - t) * yPos1 + (3 * (t*t*t) - 5 * (t*t) + 2) * yPos2 + (-3 * (t*t*t) + 4 * (t*t) + t) * yPos3 + ((t*t*t) - (t*t)) * yPos4) / 2;
//			zPos = ((-(t*t*t) + 2 * (t*t) - t) * zPos1 + (3 * (t*t*t) - 5 * (t*t) + 2) * zPos2 + (-3 * (t*t*t) + 4 * (t*t) + t) * zPos3 + ((t*t*t) - (t*t)) * zPos4) / 2;
//
//			glColor3d(0.7, 0.3, 0.7);
//			glPushMatrix();
//				glTranslatef(xPos, yPos, zPos);
//				glutWireCube(15.0);
//				glEnd();
//			glPopMatrix();
//		}
//		glPushMatrix();
//			glTranslatef(xPos2, yPos2, zPos2);
//			glutWireCube(10.0);
//		glPopMatrix();
//
//		Init();
//	}
//
//	void Init() { t = 0; xPos = 0.0; yPos = 0.0; zPos = 0.0; }
//
//private :
//	int t;
//	double xPos;
//	double yPos;
//	double zPos;
//};

// ----------------------------------- 롤러코스터 기둥 -----------------------------------
class ROLLERCOASTER
{
public:
	ROLLERCOASTER() {}
	~ROLLERCOASTER() {}

public:
	void Set_Pos(double xPos, double yPos, double zPos)
	{
		Data.xPos = xPos;
		Data.yPos = yPos;
		Data.zPos = zPos;
	}

	void Draw()
	{
		glPushMatrix();

		glTranslated(Data.xPos, -300.0, Data.zPos);
		glRotated(90.0, 1.0, 0.0, 0.0);
		glColor3d(0.45, 0.45, 0.45);
		glutSolidCylinder(20.0, -Data.yPos - 300.0, 30.0, 30.0);
		glColor3d(0.0, 0.0, 0.0);
		glutWireCylinder(22.0, -Data.yPos - 300.0, 10.0, 10.0);

		glPopMatrix();
	}

	double Get_xPos() const { return Data.xPos; }
	double Get_yPos() const { return Data.yPos; }
	double Get_zPos() const { return Data.zPos; }

private:
	SharedData Data;
};

// ----------------------------------- 장애물  제어점 -----------------------------------
class CONTROLPOINT_OF_OBSTACLE
{
public:
	CONTROLPOINT_OF_OBSTACLE() {}
	~CONTROLPOINT_OF_OBSTACLE() {}

public:
	void Draw()
	{
		glPushMatrix();

		if (Camera == OFF && View == TOP) {
			glPointSize(20.0);
			glColor3d(1.0, 0.0, 0.0);
			glBegin(GL_POINTS);
			glVertex3d(Data.xPos, 300.0, Data.zPos);
			glEnd();
		}

		glPopMatrix();
	}

	// Set함수
	void SetPos(double xPos, double yPos, double zPos)
	{
		Data.xPos = xPos;
		Data.yPos = yPos;
		Data.zPos = zPos;
	}
	void SetCollision(int state) { Collision = state; }									// Collision Set

	// Get 함수
	double Get_xPos() const { return Data.xPos; }										// xPos Get
	double Get_yPos() const { return Data.yPos; }										// yPos Get
	double Get_zPos() const { return Data.zPos; }										// zPos Get

private:
	SharedData Data;
};

// ----------------------------------- 장애물 -----------------------------------
class OBSTACLE
{
public:
	OBSTACLE() {}
	~OBSTACLE() {}

public:
	// Set 함수
	void Set_Pos(double xPos, double yPos, double zPos) { Data.xPos = xPos; Data.yPos = yPos, Data.zPos = zPos; }

	void Draw()
	{
		glPushMatrix();

		// 나무 위
		glPushMatrix();
		glTranslated(Data.xPos, Data.yPos - 100.0, Data.zPos);
		glRotated(-90.0, 1.0, 0.0, 0.0);
		glScaled(1.0, 1.0, 7.0);
		glColor3d(0.0, 1.0, 0.0);
		glutSolidCone(60, 20, 20, 20);
		glColor3d(0.25, 0.25, 0.25);
		glutWireCone(63, 20, 5.0, 5.0);

		if (Weather == SNOW) {
			glTranslated(0.0, 0.0, 15.0);
			glColor3d(1.0, 1.0, 1.0);
			glutSolidCone(18, 4, 20, 20);

			glTranslated(0.0, 0.0, -10.0);
			glutSolidCone(50, 4, 20, 20);
		}

		glPopMatrix();

		// 나무 밑
		glPushMatrix();

		glTranslated(Data.xPos, -300.0, Data.zPos);
		glRotated(90.0, 1.0, 0.0, 0.0);
		glColor3d(0.6, 0.45, 0.15);
		glutSolidCylinder(20.0, -Data.yPos - 200.0, 30.0, 30.0);
		glColor3d(1.0, 1.0, 1.0);
		glutWireCylinder(23.0, -Data.yPos - 200.0, 5.0, 5.0);

		glPopMatrix();

		glPopMatrix();
	}

private:
	SharedData Data;
	float tree_size = 1.0f;
};

// ----------------------------------- 로봇 -----------------------------------
class ROBOT
{
public:
	ROBOT(double xPos, double yPos, double zPos, double Red, double Green, double Blue)
		: Data(xPos, yPos, zPos, 0.0, 0.0, 0.0, 0.0), Dir(rand() % 4), Red(Red), Green(Green), Blue(Blue), Manteau_zPos_Mid(30.0), Manteau_zPos_End(10.0),
		RotateValue_RightLeg(0.0), RotateValue_LeftLeg(0.0), RotateValue_RightArm(0.0), RotateValue_LeftArm(0.0), Rotate_Dir(FORWARD), Rotate_Count_for_Robot_Move(0.0),
		Center_X(0.0), Center_Z(0.0), alpha(0.0), Meet(NO) {}
	~ROBOT() {}

public:
	void Draw()
	{
		Camera_Check();

		glPushMatrix();												// 로봇 그리기

			glTranslated(Data.xPos, Data.yPos, Data.zPos);
			glRotated(Data.RotateValue, Data.RotateX, Data.RotateY, Data.RotateZ);

			// 망토 그리기
			Draw_Manteau();

			glPushMatrix();											// 머리
				glTranslated(0.0, 40.0, 0.0);
				glColor3d(1.0, 0.5, 0.5);
				glutSolidCube(20.0);
			glPopMatrix();											// 머리

			glPushMatrix();											// 몸
				glTranslated(0.0, 30.0, 0.0);
				glColor3d(0.75, 0.75, 0.75);
				glutSolidCube(30.0);
			glPopMatrix();											// 몸

			glPushMatrix();											// 왼다리
				glTranslated(-10.0, 15.0, 0.0);
				glRotated(90.0, 1.0, 0.0, 0.0);
				glRotated(RotateValue_LeftLeg, 1.0, 0.0, 0.0);
				glColor3d(0.0, 1.0, 0.0);
				glutSolidCylinder(5.0, 15.0, 30.0, 30.0);
			glPopMatrix();											// 왼다리

			glPushMatrix();											// 오른다리
				glTranslated(10.0, 15.0, 0.0);
				glRotated(90.0, 1.0, 0.0, 0.0);
				glRotated(RotateValue_RightLeg, 1.0, 0.0, 0.0);
				glColor3d(0.0, 1.0, 0.0);
				glutSolidCylinder(5.0, 15.0, 30.0, 30.0);
			glPopMatrix();											// 오른다리

			glPushMatrix();											// 왼팔
				glRotated(90.0, 1.0, 0.0, 0.0);
				glTranslated(-20.0, 0.0, -40.0);
				glRotated(RotateValue_LeftArm, 1.0, 0.0, 0.0);
				glColor3d(0.0, 0.0, 1.0);
				glutSolidCylinder(5.0, 15.0, 30.0, 30.0);
			glPopMatrix();											// 왼팔

			glPushMatrix();											// 오른팔
				glRotated(90.0, 1.0, 0.0, 0.0);
				glTranslated(20.0, 0.0, -40.0);
				glRotated(RotateValue_RightArm, 1.0, 0.0, 0.0);
				glColor3d(0.0, 0.0, 1.0);
				glutSolidCylinder(5.0, 15.0, 30.0, 30.0);
			glPopMatrix();											// 오른팔

			glPushMatrix();											// 꼭다리
				glTranslated(0.0, 50.0, 0.0);
				glRotated(-90.0, 1.0, 0.0, 0.0);
				glColor3d(0.0, 0.0, 0.0);
				glutSolidCone(10.0, 10.0, 20.0, 20.0);
			glPopMatrix();											// 꼭다리

		glPopMatrix();												// 로봇 그리기
	}

	void Draw(double xPos, double zPos)
	{
		Camera_Check();

		glPushMatrix();												// 로봇 그리기

		if (Dir == BACK || Dir == FORWARD)
			glTranslated(xPos - 30.0, Data.yPos, zPos);
		else if (Dir == RIGHT || Dir == LEFT)
			glTranslated(xPos, Data.yPos, zPos - 30.0);

			glRotated(Data.RotateValue, Data.RotateX, Data.RotateY, Data.RotateZ);

			// 망토 그리기
			Draw_Manteau();

			glPushMatrix();											// 머리
				glTranslated(0.0, 40.0, 0.0);
				glColor3d(1.0, 0.5, 0.5);
				glutSolidCube(20.0);
			glPopMatrix();											// 머리

			glPushMatrix();											// 몸
				glTranslated(0.0, 30.0, 0.0);
				glColor3d(0.75, 0.75, 0.75);
				glutSolidCube(30.0);
				glPopMatrix();										// 몸

			glPushMatrix();											// 왼다리
				glTranslated(-10.0, 15.0, 0.0);
				glRotated(90.0, 1.0, 0.0, 0.0);
				glRotated(RotateValue_LeftLeg, 1.0, 0.0, 0.0);
				glColor3d(0.0, 1.0, 0.0);
				glutSolidCylinder(5.0, 15.0, 30.0, 30.0);
			glPopMatrix();											// 왼다리

			glPushMatrix();											// 오른다리
				glTranslated(10.0, 15.0, 0.0);
				glRotated(90.0, 1.0, 0.0, 0.0);
				glRotated(RotateValue_RightLeg, 1.0, 0.0, 0.0);
				glColor3d(0.0, 1.0, 0.0);
				glutSolidCylinder(5.0, 15.0, 30.0, 30.0);
			glPopMatrix();											// 오른다리

			glPushMatrix();											// 왼팔
				glRotated(90.0, 1.0, 0.0, 0.0);
				glTranslated(-20.0, 0.0, -40.0);
				glRotated(RotateValue_LeftArm, 1.0, 0.0, 0.0);
				glColor3d(0.0, 0.0, 1.0);
				glutSolidCylinder(5.0, 15.0, 30.0, 30.0);
			glPopMatrix();											// 왼팔

			glPushMatrix();											// 오른팔
				glRotated(90.0, 1.0, 0.0, 0.0);
				glTranslated(20.0, 0.0, -40.0);
				glRotated(RotateValue_RightArm, 1.0, 0.0, 0.0);
				glColor3d(0.0, 0.0, 1.0);
				glutSolidCylinder(5.0, 15.0, 30.0, 30.0);
			glPopMatrix();											// 오른팔

			glPushMatrix();											// 꼭다리
				glTranslated(0.0, 50.0, 0.0);
				glRotated(-90.0, 1.0, 0.0, 0.0);
				glColor3d(0.0, 0.0, 0.0);
				glutSolidCone(10.0, 10.0, 20.0, 20.0);
			glPopMatrix();											// 꼭다리

		glPopMatrix();												// 로봇 그리기
	}


	// 망토 그리기
	void Draw_Manteau()
	{
		GLfloat ctrlpoints[3][3][3] = {
			{{-15.0, 40.0, 15.0}, {0.0, 35.0, 15.0}, {15.0, 40.0, 15.0}},
			{{-15.0, 20.0, Manteau_zPos_Mid}, {0.0, 20.0, Manteau_zPos_Mid - 10.0}, {15.0, 20.0, Manteau_zPos_Mid + 5.0}},
			{{-15.0, 0.0, Manteau_zPos_End - 5.0}, {0.0, 0.0, Manteau_zPos_End + 10.0}, {15.0, 0.0, Manteau_zPos_End}}
		};

		if (GameStart == ON) {
			glPushMatrix();

			glColor3d(Red, Green, Blue);
			glMap2f(GL_MAP2_VERTEX_3, 0.0, 1.0, 3, 3, 0.0, 1.0, 9, 3, &ctrlpoints[0][0][0]);
			glEnable(GL_MAP2_VERTEX_3);
			// 그리드를 이용한 곡면 드로잉
			glMapGrid2f(10, 0.0, 1.0, 10, 0.0, 1.0);
			// 선을 이용하여 그리드 연결 
			glEvalMesh2(GL_LINE, 0, 10, 0, 10);

			glPopMatrix();
		}
	}

	void Manteau_DirCheck()
	{
		if (Manteau_zPos_Mid >= 60.0)
			Manteau_Mid_Dir = BACK;
		else if (Manteau_zPos_Mid <= 25.0)
			Manteau_Mid_Dir = FORWARD;

		if (Manteau_zPos_End >= 30.0)
			Manteau_End_Dir = BACK;
		else if (Manteau_zPos_End <= 0.0)
			Manteau_End_Dir = FORWARD;
	}

	void Camera_Check()
	{
		Center_Z = Center_X = 0.0;
		alpha = 30.0;

		if (Dir == FORWARD) {
			Center_Z = -120.0;
			alpha = -30.0;
		}
		else if (Dir == BACK)
			Center_Z = 120.0;
		else if (Dir == RIGHT)
			Center_X = 120.0;
		else if (Dir == LEFT)
			Center_X = -120.0;
	}

	void Set_Manteau_zPos_Mid(double Value) { Manteau_zPos_Mid += Value; }
	void Set_Manteau_zPos_End(double Value) { Manteau_zPos_End += Value; }

	void Set_Dir(int Move) { Dir = Move; }
	void Set_Rotate(double Value, double x, double y, double z)
	{
		Data.RotateX = x, Data.RotateY = y; Data.RotateZ = z;
		Data.RotateValue = Value;
	}
	void Set_Rotate_for_Move()
	{
		RotateValue_RightLeg += Rotate_Count_for_Robot_Move, RotateValue_LeftLeg -= Rotate_Count_for_Robot_Move;
		RotateValue_RightArm -= Rotate_Count_for_Robot_Move, RotateValue_LeftArm += Rotate_Count_for_Robot_Move;
	}
	void Set_xPos(double xPos) { Data.xPos -= xPos; }
	void Set_zPos(double zPos) { Data.zPos -= zPos; }

	void Set_Rotate_Count_for_Robot_Move(double a) { Rotate_Count_for_Robot_Move += a; }
	void Set_Rotate_Dir(int a) { Rotate_Dir = a; }

	void Set_Meet(int a) { Meet = a; }

	int Get_Dir() const { return Dir; }
	int Get_Rotate_Dir() const { return Rotate_Dir; }
	int Get_Manteau_Mid_Dir() const { return Manteau_Mid_Dir; }
	int Get_Manteau_End_Dir() const { return Manteau_End_Dir; }
	int Get_Meet() const { return Meet; }

	double Get_xPos() const { return Data.xPos; }
	double Get_yPos() const { return Data.yPos; }
	double Get_zPos() const { return Data.zPos; }

	double Get_RotateValue() const { return Data.RotateValue; }
	double Get_Rotate_X() const { return Data.RotateX; }
	double Get_Rotate_Y() const { return Data.RotateY; }
	double Get_Rotate_Z() const { return Data.RotateZ; }
	double Get_Rotate_Count_for_Robot_Move() const { return Rotate_Count_for_Robot_Move; }

	double Get_Center_X() { return Center_X; }
	double Get_Center_Z() { return Center_Z; }
	double Get_alpha() { return alpha; }

private:
	SharedData Data;

	int Dir;
	int Rotate_Dir;
	int Manteau_Mid_Dir;
	int Manteau_End_Dir;
	int Meet;

	double RotateValue_RightLeg;
	double RotateValue_LeftLeg;

	double RotateValue_RightArm;
	double RotateValue_LeftArm;

	double Rotate_Count_for_Robot_Move;

	double Manteau_zPos_Mid;
	double Manteau_zPos_End;

	double Red;
	double Green;
	double Blue;

	double Center_X;
	double Center_Z;
	double alpha;
};

// ----------------------------------- 바닥 -----------------------------------
class GROUND
{
public:
	GROUND() {}
	~GROUND() {}

public:
	void Draw()
	{
		glPushMatrix();

			glBegin(GL_QUADS);
				if (Weather == SNOW)
					glColor4d(1.0, 1.0, 1.0, 1.0);
				else
					glColor4d(0.2, 0.7, 0.2, 1.0);

				glVertex3d(-600.0, -300.0, -400.0);
				glVertex3d(-600.0, -300.0, 400.0);
				glVertex3d(600.0, -300.0, 400.0);
				glVertex3d(600.0, -300.0, -400.0);
			glEnd();

		glPopMatrix();
	}

private:
	SharedData Data;
};

// ----------------------------------- 눈 비 -----------------------------------

class SNOW_RAIN
{
public:
	SNOW_RAIN() {}
	~SNOW_RAIN() {}

public:
	void SetPos(int xPos, int yPos, int zPos)
	{
		this->xPos = (double)xPos;
		this->yPos = (double)yPos;
		this->zPos = (double)zPos;
	}

	void Set_yPos(double yPos)
	{
		this->yPos -= yPos;

		if (this->yPos < -300.0)
			this->yPos = 300.0;
	}

	double Get_xPos() const
	{
		if (yPos <= 0)
			return xPos;
		else
			return -999.0;
	}
	double Get_zPos() const
	{
		if (yPos <= 0)
			return zPos;
		else
			return -999.0;
	}

	void Draw()
	{
		glPushMatrix();
		glTranslated(xPos, yPos, zPos);
		if (Weather == SNOW) {
			glColor3d(1.0, 1.0, 1.0);
			glutSolidSphere(4.0, 10.0, 10.0);
		}

		else if (Weather == RAIN) {
			glScaled(1.0, 3.0, 3.0);
			glColor3d(0.0, 0.3, 1.0);
			glutSolidSphere(1.0, 10.0, 10.0);
		}
		glPopMatrix();
	}

private:
	double xPos;
	double yPos;
	double zPos;
};

class BULLET
{
public : 
	BULLET() : Drawing(YES) {}
	~BULLET() {}

public :
	void Draw()
	{
		if (Drawing == YES) {
			glPushMatrix();

				glTranslated(xPos, yPos + 30.0, zPos);
				glScaled(2.0, 1.0, 2.0);
				glColor3d(0.0, 0.0, 0.0);
				glutSolidSphere(5.0, 5.0, 5.0);

			glPopMatrix();
		}
	}

	void Init(double xPos, double yPos, double zPos, int Dir)
	{
		this->xPos = xPos;
		this->yPos = yPos;
		this->zPos = zPos;
		this->Dir  = Dir;
	}

	void Animate(double Velocity)
	{
		if (Dir == FORWARD)
			this->zPos -= Velocity;
		else if (Dir == BACK)
			this->zPos += Velocity;
		else if (Dir == RIGHT)
			this->xPos += Velocity;
		else if (Dir == LEFT)
			this->xPos -= Velocity;
	}

	void Set_Drawing(int a) { Drawing = a; }

	double Get_xPos() const { return xPos; }
	double Get_zPos() const { return zPos; }

private :
	double xPos;
	double yPos;
	double zPos;

	int Dir;
	int Drawing;
};

GLvoid DrawScene(GLvoid);
GLvoid Reshape(int w, int h);
void TimerFunction(int value);
void Keyboard(unsigned char key, int x, int y);
void Mouse(int button, int state, int x, int y);
void Motion(int x, int y);
void PassiveMotion(int x, int y);
void Init();
void CameraReset() { far_Camera_zPos = -800.0, UpX = 0.0, UpY = 1.0, UpZ = 0.0, EyeX = 0.0, EyeY = 10.0, EyeZ = 100.0, CenterX = 0.0, CenterY = 0.0, CenterZ = 0.0; }

// ----------------------------------- 객체 선언 -----------------------------------

CONTROLPOINT_OF_OBSTACLE controlpoint_of_obstacle[NumberOfPoint];
CONTROLPOINT_OF_ROLLERCOASTER controlpoint_of_rollercoaster[NumberOfPoint];
ROLLERCOASTER rollercoaster[Number_Column_of_rollercoaster];
OBSTACLE obstacle[NumberOfPoint];
GROUND ground;
CAR car;
ROBOT robot[NumberOfRobot] = { {-100.0, -300.0, -100.0, 0.0, 0.0, 1.0}, {100.0, -300.0, 100.0, 1.0, 0.0, 0.0} };
SNOW_RAIN snow_rain[SnowRainSize];
BULLET bullet[NumberOfBullet];
//CURVE curve[NumberOfPoint - 1];

// ----------------------------------------------------------------------------------

void main(int argc, char* argv[])
{
	Init();

	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGBA | GLUT_DEPTH);
	glutInitWindowPosition(100, 100);
	glutInitWindowSize(Width, Height);
	Main_Window = glutCreateWindow("소풍 로봇");
	glutTimerFunc(100, TimerFunction, 1);
	glutDisplayFunc(DrawScene);
	glutReshapeFunc(Reshape);
	glutKeyboardFunc(Keyboard);
	glutMouseFunc(Mouse);
	glutMotionFunc(Motion);
	glutPassiveMotionFunc(PassiveMotion);
	glutMainLoop();
}

GLvoid DrawScene(GLvoid)
{
	Collsion_Something = NO;
	//GLdouble ctrlpoints0[2][3] = { {controlpoint_of_rollercoaster[0].Get_xPos(), controlpoint_of_rollercoaster[0].Get_yPos(), controlpoint_of_rollercoaster[0].Get_zPos()},
	//							   {controlpoint_of_rollercoaster[1].Get_xPos(), controlpoint_of_rollercoaster[1].Get_yPos(), controlpoint_of_rollercoaster[1].Get_zPos()} };
	//
	//GLdouble ctrlpoints1[2][3] = { {controlpoint_of_rollercoaster[1].Get_xPos(), controlpoint_of_rollercoaster[1].Get_yPos(), controlpoint_of_rollercoaster[1].Get_zPos()},
	//							   {controlpoint_of_rollercoaster[2].Get_xPos(), controlpoint_of_rollercoaster[2].Get_yPos(), controlpoint_of_rollercoaster[2].Get_zPos()} };
	//
	//GLdouble ctrlpoints2[2][3] = { {controlpoint_of_rollercoaster[2].Get_xPos(), controlpoint_of_rollercoaster[2].Get_yPos(), controlpoint_of_rollercoaster[2].Get_zPos()},
	//							   {controlpoint_of_rollercoaster[3].Get_xPos(), controlpoint_of_rollercoaster[3].Get_yPos(), controlpoint_of_rollercoaster[3].Get_zPos()} };
	//
	//GLdouble ctrlpoints3[2][3] = { {controlpoint_of_rollercoaster[3].Get_xPos(), controlpoint_of_rollercoaster[3].Get_yPos(), controlpoint_of_rollercoaster[3].Get_zPos()},
	//							   {controlpoint_of_rollercoaster[4].Get_xPos(), controlpoint_of_rollercoaster[4].Get_yPos(), controlpoint_of_rollercoaster[4].Get_zPos()} };
	//
	//GLdouble ctrlpoints4[2][3] = { {controlpoint_of_rollercoaster[4].Get_xPos(), controlpoint_of_rollercoaster[4].Get_yPos(), controlpoint_of_rollercoaster[4].Get_zPos()},
	//							   {controlpoint_of_rollercoaster[5].Get_xPos(), controlpoint_of_rollercoaster[5].Get_yPos(), controlpoint_of_rollercoaster[5].Get_zPos()} };
	//
	//GLdouble ctrlpoints5[2][3] = { {controlpoint_of_rollercoaster[5].Get_xPos(), controlpoint_of_rollercoaster[5].Get_yPos(), controlpoint_of_rollercoaster[5].Get_zPos()},
	//							   {controlpoint_of_rollercoaster[6].Get_xPos(), controlpoint_of_rollercoaster[6].Get_yPos(), controlpoint_of_rollercoaster[6].Get_zPos()} };
	//
	//GLdouble ctrlpoints6[2][3] = { {controlpoint_of_rollercoaster[6].Get_xPos(), controlpoint_of_rollercoaster[6].Get_yPos(), controlpoint_of_rollercoaster[6].Get_zPos()},
	//							   {controlpoint_of_rollercoaster[7].Get_xPos(), controlpoint_of_rollercoaster[7].Get_yPos(), controlpoint_of_rollercoaster[7].Get_zPos()} };
	//
	//GLdouble ctrlpoints7[2][3] = { {controlpoint_of_rollercoaster[7].Get_xPos(), controlpoint_of_rollercoaster[7].Get_yPos(), controlpoint_of_rollercoaster[7].Get_zPos()},
	//							   {controlpoint_of_rollercoaster[8].Get_xPos(), controlpoint_of_rollercoaster[8].Get_yPos(), controlpoint_of_rollercoaster[8].Get_zPos()} };
	//
	//GLdouble ctrlpoints8[2][3] = { {controlpoint_of_rollercoaster[8].Get_xPos(), controlpoint_of_rollercoaster[8].Get_yPos(), controlpoint_of_rollercoaster[8].Get_zPos()},
	//							   {controlpoint_of_rollercoaster[9].Get_xPos(), controlpoint_of_rollercoaster[9].Get_yPos(), controlpoint_of_rollercoaster[9].Get_zPos()} };

	//GLdouble ctrlpoints[9][2][3] = { { {controlpoint_of_rollercoaster[0].Get_xPos(), controlpoint_of_rollercoaster[0].Get_yPos(), controlpoint_of_rollercoaster[0].Get_zPos()},
	//									{controlpoint_of_rollercoaster[1].Get_xPos(), controlpoint_of_rollercoaster[1].Get_yPos(), controlpoint_of_rollercoaster[1].Get_zPos()} },
	//
	//								  { {controlpoint_of_rollercoaster[2].Get_xPos(), controlpoint_of_rollercoaster[1].Get_yPos(), controlpoint_of_rollercoaster[1].Get_zPos()},
	//									{controlpoint_of_rollercoaster[3].Get_xPos(), controlpoint_of_rollercoaster[2].Get_yPos(), controlpoint_of_rollercoaster[2].Get_zPos()} },
	//
	//								  { {controlpoint_of_rollercoaster[4].Get_xPos(), controlpoint_of_rollercoaster[2].Get_yPos(), controlpoint_of_rollercoaster[2].Get_zPos()},
	//									{controlpoint_of_rollercoaster[5].Get_xPos(), controlpoint_of_rollercoaster[3].Get_yPos(), controlpoint_of_rollercoaster[3].Get_zPos()} },
	//
	//								  { {controlpoint_of_rollercoaster[5].Get_xPos(), controlpoint_of_rollercoaster[2].Get_yPos(), controlpoint_of_rollercoaster[2].Get_zPos()},
	//									{controlpoint_of_rollercoaster[6].Get_xPos(), controlpoint_of_rollercoaster[3].Get_yPos(), controlpoint_of_rollercoaster[3].Get_zPos()} },
	//
	//								  { {controlpoint_of_rollercoaster[6].Get_xPos(), controlpoint_of_rollercoaster[3].Get_yPos(), controlpoint_of_rollercoaster[3].Get_zPos()},
	//									{controlpoint_of_rollercoaster[7].Get_xPos(), controlpoint_of_rollercoaster[4].Get_yPos(), controlpoint_of_rollercoaster[4].Get_zPos()} },
	//
	//								  { {controlpoint_of_rollercoaster[8].Get_xPos(), controlpoint_of_rollercoaster[4].Get_yPos(), controlpoint_of_rollercoaster[4].Get_zPos()},
	//									{controlpoint_of_rollercoaster[9].Get_xPos(), controlpoint_of_rollercoaster[5].Get_yPos(), controlpoint_of_rollercoaster[5].Get_zPos()} },
	//
	//								  { {controlpoint_of_rollercoaster[6].Get_xPos(), controlpoint_of_rollercoaster[5].Get_yPos(), controlpoint_of_rollercoaster[5].Get_zPos()},
	//									{controlpoint_of_rollercoaster[7].Get_xPos(), controlpoint_of_rollercoaster[6].Get_yPos(), controlpoint_of_rollercoaster[6].Get_zPos()} },
	//
	//								  { {controlpoint_of_rollercoaster[7].Get_xPos(), controlpoint_of_rollercoaster[6].Get_yPos(), controlpoint_of_rollercoaster[6].Get_zPos()},
	//									{controlpoint_of_rollercoaster[8].Get_xPos(), controlpoint_of_rollercoaster[7].Get_yPos(), controlpoint_of_rollercoaster[7].Get_zPos()} },
	//
	//								  { {controlpoint_of_rollercoaster[8].Get_xPos(), controlpoint_of_rollercoaster[7].Get_yPos(), controlpoint_of_rollercoaster[7].Get_zPos()},
	//									{controlpoint_of_rollercoaster[9].Get_xPos(), controlpoint_of_rollercoaster[8].Get_yPos(), controlpoint_of_rollercoaster[8].Get_zPos()} } };

	glClearColor(0.25, 0.9, 0.95, 1.0f);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT); // 윈도우, 깊이 버퍼 클리어 하기
	// 필요한 변환 적용 및 그리기 
	// --- 변환을 적용하기 위해서 
	// glPushMatrix 함수를 호출하여 기존의 좌표 시스템을 저장 
	// 필요한 경우 행렬 초기화 ( glLoadIdentity ( ); ) 
	// 변환 적용: 이동, 회전, 신축 등 모델에 적용 할 변환 함수를 호출한다. 
	// 변환이 끝난 후에는 원래의 좌표시스템을 다시 저장하기 위하여 glPopMatrix 함수 호출 
	// 필요한 그리기 작업을 수행한다.

	// 관측 변환: 카메라의 위치 설정 (필요한 경우, 다른 곳에 설정 가능) 

	glPushMatrix();

		if (Camera == ON) {
			if (Point_of_View == THIRD)
				gluLookAt(EyeX, EyeY, EyeZ, CenterX, CenterY, CenterZ, UpX, UpY, UpZ);
			else if (Point_of_View == ROLLER)
				gluLookAt(car.Get_Car_0_xPos(), car.Get_Car_0_yPos() + 120.0 + 40.0, car.Get_Car_0_zPos(),
					car.Get_xPos_for_Center(), car.Get_yPos_for_Center() + 90.0 + 40.0, car.Get_zPos_for_Center(),
					UpX, UpY, UpZ);
			else if (Point_of_View == ROB)
				gluLookAt(robot[0].Get_xPos(), robot[0].Get_yPos() + 50.0, robot[0].Get_zPos() + robot[0].Get_alpha(),
					robot[0].Get_xPos() + robot[0].Get_Center_X(), robot[0].Get_yPos() + 50.0, robot[0].Get_zPos() + robot[0].Get_Center_Z(),
					UpX, UpY, UpZ);

			glRotated(Camera_X, 1.0, 0.0, 0.0);
			glRotated(Camera_Y, 0.0, 1.0, 0.0);
			glRotated(Camera_Z, 0.0, 0.0, 1.0);
		}
		else if (Camera == OFF) {
			glRotated(ViewRotateX, 1.0, 0.0, 0.0);
			glRotated(ViewRotateY, 0.0, 1.0, 0.0);
			glRotated(ViewRotateZ, 0.0, 0.0, 1.0);
		}

		//for (int i = 0; i < Count_controlpoint_of_rollercoaster - 1; ++i) {
		//	glMap1d(GL_MAP1_VERTEX_3, 0.0, 1.0, 3, 4, &ctrlpoints[i][0][0]);
		//
		//	glEnable(GL_MAP1_VERTEX_3);
		//	glMapGrid1f(10.0, 0.0, 1.0);	// 매개변수 0~1 사이를 10개로 나눔 
		//	glEvalMesh1(GL_LINE, 0, 10);   // 선분으로 나눈 부분 0~10까지 선으로 그림
		//	glDisable(GL_MAP1_VERTEX_3);
		//}

		//for (int i = 0; i < Count_controlpoint_of_rollercoaster; ++i)
		//	curve[i].Draw_Cardinal_Spline(controlpoint_of_rollercoaster[i % Count_controlpoint_of_rollercoaster].Get_xPos(), controlpoint_of_rollercoaster[i % Count_controlpoint_of_rollercoaster].Get_yPos(), controlpoint_of_rollercoaster[i % Count_controlpoint_of_rollercoaster].Get_zPos(),
		//								  controlpoint_of_rollercoaster[(i + 1) % Count_controlpoint_of_rollercoaster].Get_xPos(), controlpoint_of_rollercoaster[(i + 1) % Count_controlpoint_of_rollercoaster].Get_yPos(), controlpoint_of_rollercoaster[(i + 1) % Count_controlpoint_of_rollercoaster].Get_zPos(),
		//								  controlpoint_of_rollercoaster[(i + 2) % Count_controlpoint_of_rollercoaster].Get_xPos(), controlpoint_of_rollercoaster[(i + 2) % Count_controlpoint_of_rollercoaster].Get_yPos(), controlpoint_of_rollercoaster[(i + 2) % Count_controlpoint_of_rollercoaster].Get_zPos(),
		//								  controlpoint_of_rollercoaster[(i + 3) % Count_controlpoint_of_rollercoaster].Get_xPos(), controlpoint_of_rollercoaster[(i + 3) % Count_controlpoint_of_rollercoaster].Get_yPos(), controlpoint_of_rollercoaster[(i + 3) % Count_controlpoint_of_rollercoaster].Get_zPos());

		// 카디널 스플라인
		for (int j = 0; j < Count_controlpoint_of_rollercoaster; ++j) {
			//double temp_xPos = 0.0;
			//double temp_yPos = 0.0;
			//double temp_zPos = 0.0;

			for (double i = 0; i <= 100; i += 10) {
				double t = i / 100;
				double xPos = ((-(t*t*t) + 2 * (t*t) - t) * controlpoint_of_rollercoaster[j % Count_controlpoint_of_rollercoaster].Get_xPos() + (3 * (t*t*t) - 5 * (t*t) + 2) * controlpoint_of_rollercoaster[(j + 1) % Count_controlpoint_of_rollercoaster].Get_xPos() + (-3 * (t*t*t) + 4 * (t*t) + t) * controlpoint_of_rollercoaster[(j + 2) % Count_controlpoint_of_rollercoaster].Get_xPos() + ((t*t*t) - (t*t)) * controlpoint_of_rollercoaster[(j + 4) % Count_controlpoint_of_rollercoaster].Get_xPos()) / 2;
				double yPos = ((-(t*t*t) + 2 * (t*t) - t) * controlpoint_of_rollercoaster[j % Count_controlpoint_of_rollercoaster].Get_yPos() + (3 * (t*t*t) - 5 * (t*t) + 2) * controlpoint_of_rollercoaster[(j + 1) % Count_controlpoint_of_rollercoaster].Get_yPos() + (-3 * (t*t*t) + 4 * (t*t) + t) * controlpoint_of_rollercoaster[(j + 2) % Count_controlpoint_of_rollercoaster].Get_yPos() + ((t*t*t) - (t*t)) * controlpoint_of_rollercoaster[(j + 4) % Count_controlpoint_of_rollercoaster].Get_yPos()) / 2;
				double zPos = ((-(t*t*t) + 2 * (t*t) - t) * controlpoint_of_rollercoaster[j % Count_controlpoint_of_rollercoaster].Get_zPos() + (3 * (t*t*t) - 5 * (t*t) + 2) * controlpoint_of_rollercoaster[(j + 1) % Count_controlpoint_of_rollercoaster].Get_zPos() + (-3 * (t*t*t) + 4 * (t*t) + t) * controlpoint_of_rollercoaster[(j + 2) % Count_controlpoint_of_rollercoaster].Get_zPos() + ((t*t*t) - (t*t)) * controlpoint_of_rollercoaster[(j + 4) % Count_controlpoint_of_rollercoaster].Get_zPos()) / 2;

				glPushMatrix();
				glTranslatef(xPos, yPos, zPos);

				glColor3f(0.60f, 0.45f, 0.35f);
				glutSolidSphere(35.0, 40.0, 40.0);
				glColor3f(0.0f, 0.0f, 0.0f);
				glutWireSphere(40.0, 5.0, 5.0);

				if (Count_controlpoint_of_rollercoaster == NumberOfPoint && Camera == OFF) {
					for (int k = 0; k < NumberOfPoint; ++k) {
						if (controlpoint_of_obstacle[k].Get_xPos() + 80.0 > xPos &&
							controlpoint_of_obstacle[k].Get_xPos() - 80.0 < xPos &&
							controlpoint_of_obstacle[k].Get_zPos() + 80.0 > zPos &&
							controlpoint_of_obstacle[k].Get_zPos() - 80.0 < zPos) {
							glColor3f(0.25, 0.25, 0.25);
							glutSolidSphere(80.0, 40.0, 40.0);
							Collsion_Something = YES;
						}
					}
				}


				// 터널
				if (j == CreateTunnel) {
					double next_t = (i + 1) / 100;
					double next_x = ((-(next_t*next_t*next_t) + 2 * (next_t * next_t) - next_t) * controlpoint_of_rollercoaster[j % Count_controlpoint_of_rollercoaster].Get_xPos() + (3 * (next_t*next_t*next_t) - 5 * (next_t*next_t) + 2) * controlpoint_of_rollercoaster[(j + 1) % Count_controlpoint_of_rollercoaster].Get_xPos() + (-3 * (next_t*next_t*next_t) + 4 * (next_t*next_t) + next_t) * controlpoint_of_rollercoaster[(j + 2) % Count_controlpoint_of_rollercoaster].Get_xPos() + ((next_t*next_t*next_t) - (next_t*next_t)) * controlpoint_of_rollercoaster[(j + 4) % Count_controlpoint_of_rollercoaster].Get_xPos()) / 2;
					double next_y = ((-(next_t*next_t*next_t) + 2 * (next_t * next_t) - next_t) * controlpoint_of_rollercoaster[j % Count_controlpoint_of_rollercoaster].Get_yPos() + (3 * (next_t*next_t*next_t) - 5 * (next_t*next_t) + 2) * controlpoint_of_rollercoaster[(j + 1) % Count_controlpoint_of_rollercoaster].Get_yPos() + (-3 * (next_t*next_t*next_t) + 4 * (next_t*next_t) + next_t) * controlpoint_of_rollercoaster[(j + 2) % Count_controlpoint_of_rollercoaster].Get_yPos() + ((next_t*next_t*next_t) - (next_t*next_t)) * controlpoint_of_rollercoaster[(j + 4) % Count_controlpoint_of_rollercoaster].Get_yPos()) / 2;
					double next_z = ((-(next_t*next_t*next_t) + 2 * (next_t * next_t) - next_t) * controlpoint_of_rollercoaster[j % Count_controlpoint_of_rollercoaster].Get_zPos() + (3 * (next_t*next_t*next_t) - 5 * (next_t*next_t) + 2) * controlpoint_of_rollercoaster[(j + 1) % Count_controlpoint_of_rollercoaster].Get_zPos() + (-3 * (next_t*next_t*next_t) + 4 * (next_t*next_t) + next_t) * controlpoint_of_rollercoaster[(j + 2) % Count_controlpoint_of_rollercoaster].Get_zPos() + ((next_t*next_t*next_t) - (next_t*next_t)) * controlpoint_of_rollercoaster[(j + 4) % Count_controlpoint_of_rollercoaster].Get_zPos()) / 2;

					double angle_z = atan2((next_y - yPos), sqrt((next_x - xPos) * (next_x - xPos) + (next_z - zPos) * (next_z - zPos))) * 180.0 / 3.14;
					double angle_y = atan2((next_x - xPos), (next_z - zPos)) * 180.0 / 3.14;
					//double angle_x = atan2((next_z - zPos), (next_y - yPos)) * 180.0 / 3.14;

					if (i > 90) {
						glRotated(angle_z, 0.0, 0.0, 1.0);
						glRotated(angle_y, 0.0, 1.0, 0.0);
						//glRotated(angle_x, 1.0, 0.0, 0.0);
						glColor3d(0.55, 0.55, 0.55);
						glutSolidTorus(10.0, 90.0, 50.0, 50.0);
						glColor3d(0.0, 0.0, 0.0);																	
						glutWireTorus(13.0, 93.0, 5.0, 5.0);
					}
				}

				glPopMatrix();
				//temp_xPos = xPos, temp_yPos = yPos, temp_zPos = zPos;
			}
		}

		// 롤러코스터 정점 찍기
		if (Camera == OFF) {
			glPointSize(25.0);
			for (int i = 0; i < Count_controlpoint_of_rollercoaster; ++i) {
				glColor3f(0.0, 0.0, 1.0);
				if (View == TOP) {
					glBegin(GL_POINTS);
					glVertex3d(controlpoint_of_rollercoaster[i].Get_xPos(), 300.0, controlpoint_of_rollercoaster[i].Get_zPos());
					glEnd();
				}
				else if (View == SIDE) {
					glBegin(GL_POINTS);
					glVertex3d(controlpoint_of_rollercoaster[i].Get_xPos(), controlpoint_of_rollercoaster[i].Get_yPos(), 400.0);
					glEnd();
				}

				// 충돌체크 원
				glPushMatrix();

				if (View == TOP)
					glTranslated(controlpoint_of_rollercoaster[i].Get_xPos(), -300.0, controlpoint_of_rollercoaster[i].Get_zPos());

				else if (View == SIDE)
					glTranslated(controlpoint_of_rollercoaster[i].Get_xPos(), controlpoint_of_rollercoaster[i].Get_yPos(), -400.0);

				if (controlpoint_of_rollercoaster[i].GetCollision_with_obstacle() == YES) {
					Collsion_Something = YES;
					glColor3d(0.25, 0.25, 0.25);
				}
				else
					glColor3d(0.75, 0.75, 0.0);

				glutSolidSphere(80.0, 40.0, 40.0);

				glPopMatrix();
			}
		}

		// 로봇 그리기
		if (robot[0].Get_Meet() == NO) {
			for (int i = 0; i < NumberOfRobot; ++i)
				robot[i].Draw();
		}
		else if (robot[0].Get_Meet() == YES) {
			robot[0].Draw();
			robot[1].Draw(robot[0].Get_xPos(), robot[0].Get_zPos());
		}

		// 총알
		for (int i = 0; i < Count_Bullet; ++i)
			bullet[i].Draw();
		
		// 롤러코스터 기둥
		for (int i = 0; i < Count_Column_of_rollercoaster; ++i)
			rollercoaster[i].Draw();

		// 롤러코스터 열차
		if (Count_controlpoint_of_rollercoaster >= NumberOfPoint)
			car.Draw();

		// 장애물 정점
		if (Camera == OFF && View == TOP) {
			for (int i = 0; i < NumberOfPoint; ++i)
				controlpoint_of_obstacle[i].Draw();
		}

		// 장애물
		if (Camera == ON || View == SIDE) {
			for (int i = 0; i < NumberOfPoint; ++i)
				obstacle[i].Draw();
		}

		// 눈비
		if (Weather != SUNNY)
			for (int i = 0; i < SnowRainSize; ++i)
				snow_rain[i].Draw();

		// 바닥
		ground.Draw();

	glPopMatrix();

	//std::cout << "카메라 x :" << EyeX << std::endl;
	//std::cout << "열차 x :" << car.Get_Car_1_xPos() << std::endl;
	//std::cout << "열차0 y :" << car.Get_Car_0_yPos() << std::endl;
	//std::cout << "열차1 y :" << car.Get_Car_1_yPos() << std::endl;
	//std::cout << "로봇0번 Dir :" << robot[0].Get_Dir() << std::endl;

	if (Collsion_Something == NO && Camera == OFF)
		Can_game_start_about_collision = YES;
	else if (Collsion_Something == YES && Camera == OFF)
		Can_game_start_about_collision = NO;

	glutSwapBuffers();
}

GLvoid Reshape(int w, int h)
{
	// 뷰포트 변환 설정: 출력 화면 결정 
	glViewport(0, 0, w, h);
	// 클리핑 변환 설정: 출력하고자 하는 공간 결정 
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	// 원근 투영을 사용하는 경우
	if (Camera == ON) {
		gluPerspective(60.0, Width / Height, 1.0, 2000.0);
		glTranslated(0.0, 0.0, far_Camera_zPos);
	}

	// 직각 투영을 사용하는 경우
	else if (Camera == OFF)
		glOrtho(-Width / 2.0, Width / 2.0, -Height / 2.0, Height / 2.0, -400.0, 400.0);

	// 모델링 변환 설정: 디스플레이 콜백 함수에서 모델 변환 적용하기 위하여 Matrix mode 저장 
	glMatrixMode(GL_MODELVIEW);
	glEnable(GL_DEPTH_TEST);
}

void TimerFunction(int value)
{
	srand((unsigned)time(nullptr));

	// 열차 속도 조절
	//if (car.Get_Car_0_yPos() < car.Get_Car_1_yPos())
	//	car.SpeedUp();
	//else if (car.Get_Car_0_yPos() > car.Get_Car_1_yPos())
	//	car.SpeedDown(); 
	//else if (car.Get_Car_0_yPos() == car.Get_Car_1_yPos())
	//	car.SpeedSet();

	bool ColisionCheck = false;

	if (Count_controlpoint_of_rollercoaster == NumberOfPoint) {
		for (int i = 0; i < NumberOfPoint; ++i) {
			ColisionCheck = false;
			for (int j = 0; j < NumberOfPoint; ++j) {
				if (controlpoint_of_obstacle[j].Get_xPos() + 80.0 > controlpoint_of_rollercoaster[i].Get_xPos() &&
					controlpoint_of_obstacle[j].Get_xPos() - 80.0 < controlpoint_of_rollercoaster[i].Get_xPos() &&
					controlpoint_of_obstacle[j].Get_zPos() + 80.0 > controlpoint_of_rollercoaster[i].Get_zPos() &&
					controlpoint_of_obstacle[j].Get_zPos() - 80.0 < controlpoint_of_rollercoaster[i].Get_zPos()) {

					controlpoint_of_rollercoaster[i].Set_Collision_with_obstacle(YES);
					ColisionCheck = true;
				}
				else
					controlpoint_of_rollercoaster[i].Set_Collision_with_obstacle(NO);

				if (ColisionCheck == true)
					break;
			}
		}
	}

	// 열차객체로 컨트롤포인트 넘겨주기
	if (GameStart == OFF && Count_controlpoint_of_rollercoaster == NumberOfPoint) {
		for (int j = 0; j < NumberOfPoint; ++j) {
			rollercoaster_controlpoint_xPosArr[j] = controlpoint_of_rollercoaster[j].Get_xPos();
			rollercoaster_controlpoint_yPosArr[j] = controlpoint_of_rollercoaster[j].Get_yPos();
			rollercoaster_controlpoint_zPosArr[j] = controlpoint_of_rollercoaster[j].Get_zPos();
		}
		car.Set_Pos(rollercoaster_controlpoint_xPosArr, rollercoaster_controlpoint_yPosArr, rollercoaster_controlpoint_zPosArr);
	}

	for (int i = 0; i < Count_Column_of_rollercoaster; ++i)
		rollercoaster[i].Set_Pos(controlpoint_of_rollercoaster[i * 2].Get_xPos(), controlpoint_of_rollercoaster[i * 2].Get_yPos(), controlpoint_of_rollercoaster[i * 2].Get_zPos());

	if (Count_controlpoint_of_rollercoaster == NumberOfPoint)
		CAN_START = ON;

	// 로봇 충돌체크 + 움직임 체크
	if (GameStart == ON) {
		// 로봇 손 안 잡았을 때
		if (robot[0].Get_Meet() == NO) {
			for (int i = 0; i < NumberOfRobot; ++i) {
				if (robot[i].Get_Dir() != STOP) {
					robot[i].Set_Rotate_for_Move();
					if (robot[i].Get_Rotate_Dir() == FORWARD)
						robot[i].Set_Rotate_Count_for_Robot_Move(2.0);
					else if (robot[i].Get_Rotate_Dir() == BACK)
						robot[i].Set_Rotate_Count_for_Robot_Move(-2.0);
				}

				if (robot[i].Get_Rotate_Count_for_Robot_Move() > 5.0)
					robot[i].Set_Rotate_Dir(BACK);
				else if (robot[i].Get_Rotate_Count_for_Robot_Move() < -5.0)
					robot[i].Set_Rotate_Dir(FORWARD);


				for (int j = 0; j < NumberOfPoint; ++j) {
					if (sqrt((robot[i].Get_xPos() - controlpoint_of_obstacle[j].Get_xPos()) * (robot[i].Get_xPos() - controlpoint_of_obstacle[j].Get_xPos()) +
						(robot[i].Get_zPos() - controlpoint_of_obstacle[j].Get_zPos()) * (robot[i].Get_zPos() - controlpoint_of_obstacle[j].Get_zPos())) < 30.0) {
						robot[i].Set_Dir(rand() % 4);
					}
				}

				for (int k = 0; k < NumberOfPoint / 2; ++k) {
					if (sqrt((robot[i].Get_xPos() - rollercoaster[k].Get_xPos()) * (robot[i].Get_xPos() - rollercoaster[k].Get_xPos()) +
						(robot[i].Get_zPos() - rollercoaster[k].Get_zPos()) * (robot[i].Get_zPos() - rollercoaster[k].Get_zPos())) < 30.0) {
						robot[i].Set_Dir(rand() % 4);
					}
				}

				if (robot[i].Get_xPos() > 400.0 || robot[i].Get_xPos() < -400.0 ||
					robot[i].Get_zPos() > 400.0 || robot[i].Get_zPos() < -400.0)
					robot[i].Set_Dir(rand() % 4);
			}
		}
		// 로봇 손잡았을 때
		else if (robot[0].Get_Meet() == YES) {
			if (robot[0].Get_Dir() != STOP) {
				robot[0].Set_Rotate_for_Move();
				if (robot[0].Get_Rotate_Dir() == FORWARD) {
					robot[0].Set_Rotate_Count_for_Robot_Move(2.0);
					robot[1].Set_Rotate_Count_for_Robot_Move(2.0);
				}

				else if (robot[0].Get_Rotate_Dir() == BACK) {
					robot[0].Set_Rotate_Count_for_Robot_Move(-2.0);
					robot[1].Set_Rotate_Count_for_Robot_Move(-2.0);
				}
			}

			if (robot[0].Get_Rotate_Count_for_Robot_Move() > 5.0) {
				robot[0].Set_Rotate_Dir(BACK);
				robot[1].Set_Rotate_Dir(BACK);
			}
			else if (robot[0].Get_Rotate_Count_for_Robot_Move() < -10.0) {
				robot[0].Set_Rotate_Dir(FORWARD);
				robot[1].Set_Rotate_Dir(FORWARD);
			}


			for (int j = 0; j < NumberOfPoint; ++j) {
				if (sqrt((robot[0].Get_xPos() - controlpoint_of_obstacle[j].Get_xPos()) * (robot[0].Get_xPos() - controlpoint_of_obstacle[j].Get_xPos()) +
						 (robot[0].Get_zPos() - controlpoint_of_obstacle[j].Get_zPos()) * (robot[0].Get_zPos() - controlpoint_of_obstacle[j].Get_zPos())) < 30.0) {
					robot[0].Set_Dir(rand() % 4);
					robot[1].Set_Dir(robot[0].Get_Dir());
				}
				if (sqrt((robot[1].Get_xPos() - controlpoint_of_obstacle[j].Get_xPos()) * (robot[1].Get_xPos() - controlpoint_of_obstacle[j].Get_xPos()) +
						 (robot[1].Get_zPos() - controlpoint_of_obstacle[j].Get_zPos()) * (robot[1].Get_zPos() - controlpoint_of_obstacle[j].Get_zPos())) < 30.0) {
					robot[0].Set_Dir(rand() % 4);
					robot[1].Set_Dir(robot[0].Get_Dir());
				}
			}

			for (int k = 0; k < NumberOfPoint / 2; ++k) {
				if (sqrt((robot[0].Get_xPos() - rollercoaster[k].Get_xPos()) * (robot[0].Get_xPos() - rollercoaster[k].Get_xPos()) +
						 (robot[0].Get_zPos() - rollercoaster[k].Get_zPos()) * (robot[0].Get_zPos() - rollercoaster[k].Get_zPos())) < 30.0) {
					robot[0].Set_Dir(rand() % 4);
					robot[1].Set_Dir(robot[0].Get_Dir());
				}
				if (sqrt((robot[1].Get_xPos() - rollercoaster[k].Get_xPos()) * (robot[1].Get_xPos() - rollercoaster[k].Get_xPos()) +
						 (robot[1].Get_zPos() - rollercoaster[k].Get_zPos()) * (robot[1].Get_zPos() - rollercoaster[k].Get_zPos())) < 30.0) {
					robot[0].Set_Dir(rand() % 4);
					robot[1].Set_Dir(robot[0].Get_Dir());
				}
			}

			if (robot[0].Get_xPos() > 400.0 || robot[0].Get_xPos() < -400.0 ||
				robot[0].Get_zPos() > 400.0 || robot[0].Get_zPos() < -400.0) {

				robot[0].Set_Dir(rand() % 4);
				robot[1].Set_Dir(robot[0].Get_Dir());
			}
			if (robot[1].Get_xPos() > 400.0 || robot[1].Get_xPos() < -400.0 ||
				robot[1].Get_zPos() > 400.0 || robot[1].Get_zPos() < -400.0) {

				robot[0].Set_Dir(rand() % 4);
				robot[1].Set_Dir(robot[0].Get_Dir());
			}
		}

		for (int i = 0; i < NumberOfRobot; ++i) {
			if (robot[i].Get_Dir() == FORWARD) {
				robot[i].Set_Rotate(0.0, 0.0, 0.0, 0.0);
				robot[i].Set_zPos(5.0);
			}

			else if (robot[i].Get_Dir() == BACK) {
				robot[i].Set_Rotate(180.0, 0.0, 1.0, 0.0);
				robot[i].Set_zPos(-5.0);
			}

			else if (robot[i].Get_Dir() == RIGHT) {
				robot[i].Set_Rotate(-90.0, 0.0, 1.0, 0.0);
				robot[i].Set_xPos(-5.0);
			}

			else if (robot[i].Get_Dir() == LEFT) {
				robot[i].Set_Rotate(90.0, 0.0, 1.0, 0.0);
				robot[i].Set_xPos(5.0);
			}

			// 망토 펄럭
			robot[i].Manteau_DirCheck();

			if (robot[i].Get_Manteau_Mid_Dir() == FORWARD)
				robot[i].Set_Manteau_zPos_Mid(3.0);
			else if (robot[i].Get_Manteau_Mid_Dir() == BACK)
				robot[i].Set_Manteau_zPos_Mid(-3.0);

			if (robot[i].Get_Manteau_End_Dir() == FORWARD)
				robot[i].Set_Manteau_zPos_End(10.0);
			else if (robot[i].Get_Manteau_End_Dir() == BACK)
				robot[i].Set_Manteau_zPos_End(-10.0);
		}
	}

	// 로봇끼리 충돌
	if (robot[0].Get_xPos() + 40.0 > robot[1].Get_xPos() && robot[0].Get_xPos() - 40.0 < robot[1].Get_xPos() &&
		robot[0].Get_zPos() + 40.0 > robot[1].Get_zPos() && robot[0].Get_zPos() - 40.0 < robot[1].Get_zPos()) {

		robot[0].Set_Meet(YES);
		robot[1].Set_Meet(YES);

		robot[1].Set_Dir(robot[0].Get_Dir());
	}

	// 총알
	if (Count_Bullet >= 0) {
		bullet[Count_Bullet].Init(robot[0].Get_xPos(), robot[0].Get_yPos(), robot[0].Get_zPos(), robot[0].Get_Dir());
		for (int i = 0; i < Count_Bullet; ++i)
			bullet[i].Animate(Bullet_Velocity);
	}
	
	// 총알 충돌
	for (int i = 0; i < NumberOfBullet; ++i) {
		for (int j = 0; j < NumberOfPoint; ++j) {
			if (sqrt((bullet[i].Get_xPos() - controlpoint_of_obstacle[j].Get_xPos()) * (bullet[i].Get_xPos() - controlpoint_of_obstacle[j].Get_xPos()) +
					 (bullet[i].Get_zPos() - controlpoint_of_obstacle[j].Get_zPos()) * (bullet[i].Get_zPos() - controlpoint_of_obstacle[j].Get_zPos())) < 30.0) {
				bullet[i].Set_Drawing(NO);
			}
		}

		for (int k = 0; k < NumberOfPoint / 2; ++k) {
			if (sqrt((bullet[i].Get_xPos() - rollercoaster[k].Get_xPos()) * (bullet[i].Get_xPos() - rollercoaster[k].Get_xPos()) +
					 (bullet[i].Get_zPos() - rollercoaster[k].Get_zPos()) * (bullet[i].Get_zPos() - rollercoaster[k].Get_zPos())) < 30.0) {
				bullet[i].Set_Drawing(NO);
			}
		}

		if (bullet[i].Get_xPos() > 450.0 || bullet[i].Get_xPos() < -450.0 ||
			bullet[i].Get_zPos() > 450.0 || bullet[i].Get_zPos() < -450.0)
			bullet[i].Set_Drawing(NO);

		if (robot[1].Get_xPos() + 30.0 > bullet[i].Get_xPos() && robot[1].Get_xPos() - 30.0 < bullet[i].Get_xPos() &&
			robot[1].Get_zPos() + 30.0 > bullet[i].Get_zPos() && robot[1].Get_zPos() - 30.0 < bullet[i].Get_zPos())
			bullet[i].Set_Drawing(NO);
	}

	// 눈내리기
	if (Weather != SUNNY) {
		for (int i = 0; i < SnowRainSize; ++i) {
			if (Weather == SNOW)
				snow_rain[i].Set_yPos(5.0);
			else if (Weather == RAIN)
				snow_rain[i].Set_yPos(10.0);
		}
	}

	glutPostRedisplay(); // 화면 재출력을 위하여 디스플레이 콜백 함수 호출 
	glutTimerFunc(100, TimerFunction, 1);
}

void Keyboard(unsigned char key, int x, int y)
{
	switch (key) {
	// 탑뷰 점찍기
	case '1':
		if (GameStart == OFF) {
			View = TOP;
			ViewRotateX = 90.0;
			ViewRotateY = 0.0;
			ViewRotateZ = 0.0;
		}
		break;
	// 사이드뷰 점찍기
	case '2':
		if (GameStart == OFF) {
			View = SIDE;
			ViewRotateX = 0.0;
			ViewRotateY = 0.0;
			ViewRotateZ = 0.0;
		}
		break;

	// 게임 시작
	case '3':
		if (CAN_START == ON && Can_game_start_about_collision == YES) {
			GameStart = ON;
			Camera = ON;
		}
		Reshape(Width, Height);
		break;

	// 장애물 위치 초기화
	case 'o' :
	case 'O' :
		if (Camera == OFF)
			Init();
		break;

	// 카메라 축 회전
	case 'x' :
		Camera_X += 10.0;
		break;
	case 'X' :
		Camera_X -= 10.0;
		break;
	case 'y' :
		Camera_Y += 10.0;
		break;
	case 'Y':
		Camera_Y -= 10.0;
		break;
	case 'z':
		EyeZ -= 10.0;
		break;
	case 'Z':
		EyeZ += 10.0;
		break;

	// 시점 변경
	case 'v':
		if (Point_of_View == THIRD) {
			Point_of_View = ROLLER;
			far_Camera_zPos = 0.0;
		}
		else if (Point_of_View == ROLLER) {
			Point_of_View = ROB;
			far_Camera_zPos = 0.0;
		}
		else if (Point_of_View == ROB) {
			Point_of_View = THIRD;
			far_Camera_zPos = -800.0;
			CameraReset();
		}
		Reshape(Width, Height);
		Camera_X = Camera_Y = Camera_Z = 0.0;
		break;
	case 'V':
		if (Point_of_View == THIRD) {
			Point_of_View = ROB;
			far_Camera_zPos = 0.0;
		}
		else if (Point_of_View == ROLLER) {
			Point_of_View = THIRD;
			far_Camera_zPos = -800.0;
			CameraReset();
		}
		else if (Point_of_View == ROB) {
			Point_of_View = ROLLER;
			far_Camera_zPos = 0.0;
		}
		Reshape(Width, Height);
		Camera_X = Camera_Y = Camera_Z = 0.0;
		break;

		// 줌인아웃
		//case '+' :
		//	if (Point_of_View = THIRD)
		//		EyeZ -= 5.0;
		//	break;
		//case '-' :
		//	if (Point_of_View = THIRD)
		//		EyeZ += 5.0;
		//	break;

	// 로봇의 움직임
	case 'w':
		robot[0].Set_Dir(FORWARD);
		if (robot[0].Get_Meet() == YES)
			robot[1].Set_Dir(FORWARD);
		break;
	case 'a':
		robot[0].Set_Dir(LEFT);
		if (robot[0].Get_Meet() == YES)
			robot[1].Set_Dir(LEFT);
		break;
	case 's':
		robot[0].Set_Dir(BACK);
		if (robot[0].Get_Meet() == YES)
			robot[1].Set_Dir(BACK);
		break;
	case 'd':
		robot[0].Set_Dir(RIGHT);
		if (robot[0].Get_Meet() == YES)
			robot[1].Set_Dir(RIGHT);
		break;
	
	// 총알 발사
	case 'g' :
	case 'G' :
		Count_Bullet = (Count_Bullet + 1) % NumberOfBullet;
		bullet[Count_Bullet].Set_Drawing(YES);
		break;

	// 날씨
	case 'k':
		if (Weather == SNOW)
			Weather = SUNNY;
		else if (Weather == SUNNY)
			Weather = RAIN;
		else if (Weather == RAIN)
			Weather = SNOW;
		break;

	// 종료
	case 'q':
	case 'Q':
		glutDestroyWindow(Main_Window);
		exit(0);
		break;
	}

	glutPostRedisplay();
}

void Mouse(int button, int state, int x, int y)
{
	if (button == GLUT_LEFT_BUTTON && state == GLUT_DOWN && View == TOP && Count_controlpoint_of_rollercoaster < NumberOfPoint) {
		controlpoint_of_rollercoaster[Count_controlpoint_of_rollercoaster].Set_xPos(x);
		controlpoint_of_rollercoaster[Count_controlpoint_of_rollercoaster].Set_yPos(300.0);
		controlpoint_of_rollercoaster[Count_controlpoint_of_rollercoaster++].Set_zPos(y);
		if (Count_controlpoint_of_rollercoaster % 2 == 0)
			++Count_Column_of_rollercoaster;
	}

	if (button == GLUT_LEFT_BUTTON && state == GLUT_DOWN && View == SIDE && Count_controlpoint_of_rollercoaster < NumberOfPoint) {
		controlpoint_of_rollercoaster[Count_controlpoint_of_rollercoaster].Set_xPos(x);
		controlpoint_of_rollercoaster[Count_controlpoint_of_rollercoaster].Set_yPos(y);
		controlpoint_of_rollercoaster[Count_controlpoint_of_rollercoaster++].Set_zPos(400.0);
		if (Count_controlpoint_of_rollercoaster % 2 == 0)
			++Count_Column_of_rollercoaster;
	}

	if (button == GLUT_RIGHT_BUTTON && state == GLUT_DOWN) {
		RightMouse = DOWN;
		for (int i = 0; i < Count_controlpoint_of_rollercoaster; ++i)
			controlpoint_of_rollercoaster[i].CollisionWithMouse(x, y);
	}

	if (button == GLUT_RIGHT_BUTTON && state == GLUT_UP) {
		RightMouse = UP;
		for (int i = 0; i < Count_controlpoint_of_rollercoaster; ++i)
			controlpoint_of_rollercoaster[i].SetCollision(NO);
	}
}

void Motion(int x, int y)
{
	if (RightMouse == DOWN) {
		for (int i = 0; i < Count_controlpoint_of_rollercoaster; ++i) {
			if (controlpoint_of_rollercoaster[i].GetCollision() == YES)
				controlpoint_of_rollercoaster[i].MovePos(x, y);
		}
	}
}

void PassiveMotion(int x, int y)
{
	double Mouse_X = (double)x - Width / 2;
	double Mouse_Y = Height / 2 - (double)y;

	EyeX = Mouse_X;
	EyeY = Mouse_Y;
}

// ----------------------------------- 장애물 위치 초기값 -----------------------------------
void Init()
{
	srand((unsigned)time(nullptr));

	for (int i = 0; i < NumberOfPoint; ++i) {
		controlpoint_of_obstacle[i].SetPos((rand() % 60 - 30) * 10, (rand() % 5 - 4) * 10, (rand() % 60 - 30) * 10);
		obstacle[i].Set_Pos(controlpoint_of_obstacle[i].Get_xPos(), controlpoint_of_obstacle[i].Get_yPos(), controlpoint_of_obstacle[i].Get_zPos());
	}

	for (int i = 0; i < SnowRainSize; ++i)
		snow_rain[i].SetPos((rand() % 80 - 40) * 10, (rand() % 60 - 30) * 10, (rand() % 80 - 40) * 10);

	CreateTunnel = rand() % NumberOfPoint;
}